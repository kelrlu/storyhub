
// @GENERATOR:play-routes-compiler
// @SOURCE:/Users/kelurulu/Documents/Columbia Classes/Fall 2015/Software Engineering/storyhub/conf/routes
// @DATE:Sat Dec 12 21:29:00 EST 2015


package router {
  object RoutesPrefix {
    private var _prefix: String = "/"
    def setPrefix(p: String): Unit = {
      _prefix = p
    }
    def prefix: String = _prefix
    val byNamePrefix: Function0[String] = { () => prefix }
  }
}
